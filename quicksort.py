def sort(array, low, high):
    if low < high:	
        pivot = partition(array, low, high)
        sort(array, low, pivot-1)
        sort(array, pivot+1, high)

def partition(array, low, high):
    pivot = array[high]
    i = low
    for j in range(low, high):
        if array[j] < pivot:
            array[i], array[j] = array[j], array[i]
            i += 1
    array[i], array[high] = array[high], array[i]
    return i
