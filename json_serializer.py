class JSONSerDes:

    whitespaces = ' \n\t\v\r'

    @classmethod
    def loads(cls, json_string):
        
        json_string = json_string.strip(cls.whitespaces)
        if cls.is_valid_json_string(json_string):
            python_dict = cls.form_dict(json_string)
        else:
            print("Failed. Check you data.")
            return None
        return python_dict

    @classmethod
    def is_valid_json_string(cls, json_string):
        
        if not (json_string.startswith('{')
                and json_string.endswith('}')):
            return False

        json_puncts = [':', ',', '[', ']', '{', '}']
        my_puncts = {}
        for char in json_string:
            if char in json_puncts:
                if not my_puncts.get(char):
                    my_puncts[char] = 1
                else:
                    my_puncts[char] += 1
        if my_puncts['{'] != my_puncts['}']:
            return False
        if my_puncts['['] != my_puncts[']']:
            return False
        return True

    @classmethod
    def form_dict(cls, json_string):
        json_string = json_string[1:-1]        # Removing brackets
        json_string = json_string.strip(cls.whitespaces)
        result = {}
        while json_string:
            parts = json_string.split(',', maxsplit=1)
            if '[' in parts[0]:
                rindex = cls.find_bracket_ending(json_string, '[', ']', json_string.index('['))
                key_value = parts[0].split(':')      # Potential troubles
                result[cls.eval_value(key_value[0].strip(cls.whitespaces))] = \
                                                cls.form_list(json_string[json_string.index('['):rindex])
                json_string = json_string[rindex+1:].strip(cls.whitespaces)      
            elif '{' in parts[0]:
                rindex = cls.find_bracket_ending(json_string, '{', '}', json_string.index('{'))
                key_value = parts[0].split(':')      # Potential troubles
                result[cls.eval_value(key_value[0].strip(cls.whitespaces))] = \
                                                cls.form_dict(json_string[json_string.index('{'):rindex])
                json_string = json_string[rindex+1:].strip(cls.whitespaces)
            else:
                key_value = parts[0].split(':')
                result[cls.eval_value(key_value[0].strip(cls.whitespaces))] = \
                                                cls.eval_value(key_value[1].strip(cls.whitespaces))        # Could be problems!!!
                try:
                    json_string = parts[1].strip(cls.whitespaces)
                except IndexError:
                    json_string = ''
            if json_string:        
                if json_string[0] == ',':
                    json_string = json_string[1:].strip(cls.whitespaces)
        return result

    @classmethod
    def form_list(cls, json_string):
        json_string = json_string[1:-1]        # Removing brackets
        json_string = json_string.strip(cls.whitespaces)
        result = []
        while json_string:
            parts = json_string.split(',', maxsplit=1)
            if '[' in parts[0]:
                rindex = cls.find_bracket_ending(json_string, '[', ']', json_string.index('['))
                result.append(cls.form_list(json_string[:rindex].strip(cls.whitespaces)))
                json_string = json_string[rindex+1:].strip(cls.whitespaces)
            elif '{' in parts[0]:
                rindex = cls.find_bracket_ending(json_string, '{', '}', json_string.index('{'))
                result.append(cls.form_dict(json_string[:rindex].strip(cls.whitespaces)))
                json_string = json_string[rindex+1:].strip(cls.whitespaces)
            else:
                result.append(cls.eval_value(parts[0].strip(cls.whitespaces)))
                try:
                    json_string = parts[1].strip(cls.whitespaces)
                except IndexError:
                    json_string = ''
            if json_string:
                if json_string[0] == ',':
                    json_string = json_string[1:].strip(cls.whitespaces)
        return result
    
    @classmethod
    def find_bracket_ending(cls, string_var, opening_char, closing_char, beginning):
        counter = 0   # Every time after first step it will be 1
        pos = 0
        for char in string_var[beginning:]:
            if char == opening_char:
                counter += 1
            if char == closing_char:
                counter -= 1
            if counter == 0:
                return beginning + pos + 1
            pos += 1
        return None

    @classmethod
    def eval_value(cls, string_var):
        chars = '"\''
        string_var = string_var.strip(chars)
        if string_var.isdecimal():
            return int(string_var)
        try:
            float(string_var)
        except ValueError:
            pass
        else:
            return float(string_var)
        if string_var == 'true':
            return True
        elif string_var == 'false':
            return False
        elif string_var == 'null':
            return None
        else:
            return string_var

    #python to json
    @classmethod
    def dumps(cls, var):
        pass

    @classmethod
    def prepare_for_dump_dict(cls, var):
        if type(var) != dict:
            print('Failed. Check your data.')
            return None
        return var
